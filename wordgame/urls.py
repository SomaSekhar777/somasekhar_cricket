from django.urls import path
from . import views

urlpatterns = [
    path("sayhello/", views.say_hello, name = 'say_hello'),
    path('somu/', views.show_html, name = 'show_html')
]