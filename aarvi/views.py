from django.shortcuts import render
from django.http import HttpResponse
import random
# Create your views here.

R_P_S = ["rock", "paper", "scissors"]

def computer_guess() -> str:
    return random.choice(R_P_S)

def play_rock_paper_scissors(p1: str, p2: str) -> str:
    if p1.lower() == p2.lower():
        return "draw"
    outcomes = {("rock", "paper"): "paper",
                ("scissors", "paper"): "scissors",
                ("scissors", "rock"): "rock"}
    inputs = (max(p1, p2), min(p1, p2))
    return outcomes[inputs]

def say_hello(request):
    return HttpResponse('''
    <!doctype html>
    <html>
        <body>
            <h3>Greetings</h3>
            <p>Hello, World!</p>
        </body>
    </html>''')

def play_rps(request, user_choice):
    #user_choice = request.GET['user_choice']
    #user_choice = request.GET.get('user_choice', 'rock')
    computer_choice = computer_guess()
    outcome = play_rock_paper_scissors(user_choice, computer_choice)
    context = {"user": user_choice,
               "computer": computer_choice,
               "outcome": outcome}
    return render(request, "hello/rps.html", context)

def show_rps_form(request):
    return render(request, "hello/rps_form.html")

def process_rps_form(request):
    user_choice = request.POST['user_choice']
    computer_choice = computer_guess()
    outcome = play_rock_paper_scissors(user_choice, computer_choice)
    context = {"user": user_choice,
               "computer": computer_choice,
               "outcome": outcome}
    return render(request, "hello/rps.html", context)