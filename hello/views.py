from django.shortcuts import render
from django.http import HttpResponse
from django.core.files import File
import random
def say_hello(request):
    return HttpResponse('''
    <!doctype html>
    <html>
        <body>
            <h3>Greetings</h3>
            <p>Hello, World!</p>
        </body>
    </html>''')


    
R_P_S = ["rock", "paper", "scissors"]

def computer_guess() -> str:
    return random.choice(R_P_S)

def play_rock_paper_scissors(p1: str, p2: str) -> str:
    if p1.lower() == p2.lower():
        return "draw"
    outcomes = {("rock", "paper"): "paper",
                ("scissors", "paper"): "scissors",
                ("scissors", "rock"): "rock"}
    inputs = (max(p1, p2), min(p1, p2))
    return outcomes[inputs]


def play_rps(request, user_choice):
    #user_choice = request.GET['user_choice']
    #user_choice = request.GET.get('user_choice', 'rock')
    computer_choice = computer_guess()
    outcome = play_rock_paper_scissors(user_choice, computer_choice)
    return HttpResponse(f'''
    <!doctype html>
    <html>
        <body>
            <h3>Rock-Paper-Scissors</h3>
            <p>You Chose {user_choice}</p>
            <p>Computer chose {computer_choice}</p>
            <p>Winner: {outcome}</p>
        </body>
    </html>
    ''')

qa =[ "what is your name?"
        "father name?",
        "mother name?" ]

qu ={"what is your name?" : "1" ,
        "father name?": '2' ,
        "mother name?" : "3"}


a = ""
list1= []

def show_question(request):
    
    if request.method == "POST":
        input_ = request.POST["user_answer"]
        d = list1[0]
        if qu[d] == input_ :

            context2 = { "question" : d,
                         "your_answer" : input_ ,
                         "result" : "correct Answer"
            }
            return render(request, "hello/pass.html", context2)
        else:
            actual_ans = qu[d]
            context3 = { "question" :  d,
                         "your_answer" : input_ ,
                         "act_answer" : actual_ans,
                         "result" : "wrong answer"
            }
            return render(request, "hello/fail.html", context3)
    else:
        random_q = random.choice(list(qu.keys()))
        list1.append(random_q)
        context = {
        "question" : random_q
        }
        return render(request, "hello/first.html", context)
    




chosen_question =[]
def show_question_(request):
    random_q = random.choice(qu.keys())
    chosen_question.append(random_q)
    context = {
        "question" : random_q
        }
    return render(request, "hello/first.html", context)

def process_quiz_form_ (request):
    input_ = request.POST["user_answer"]
    question_ = chosen_question.pop(0)
    if qu[question_] == input_ :

            context2 = { "question" : question_,
                         "your_answer" : input_ ,
                         "result" : "correct Answer"
            }
            return render(request, "hello/pass.html", context2)
    else:
            actual_ans = qu[question_]
            context3 = { "question" :   question_,
                         "your_answer" : input_ ,
                         "act_answer" : actual_ans,
                         "result" : "wrong answer"
            }
            return render(request, "hello/fail.html", context3)
    

#def text_viewer (request):

    #f = open("C:/Users/LENOVO\Desktop/summary.txt", "r")
    #line = f.readlines()
    #context1 = {
   #     "text" : line
  #  }
#
 #   return render(request, 'hello/line.html', context1)


words_list = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
            'aa','aab','bb','cc','dd','ee','ff','gg','hh','ii','jj','kk','ll','mm','nn','oo','pp','qq','rr','ss','tt','uu','vv','ww','xx','yy','zz',
            'aaa','bbb','ccc','ddd','eee','fff','ggg','hhh','iii','jjj','kkk','lll','mmm','nnn','ooo','ppp','qqq','rrr','sss','ttt','uuu','vvv','www','xxx','yyy','zzz',
            'aaaa','bbbb','cccc','dddd','eeee','ffff','gggg','hhhh','iiii','jjjj','kkkk','llll','mmmm','nnnn','oooo','pppp','qqqq','rrrr','ssss','tttt','uuuu','vvvv','wwww','xxxx','yyyy','zzzz']
def word_ (request):
    return render(request, 'hello/enter.html')

def process_ (request):
    number_of_letters = int(request.POST["number_length"])
    #if request.session.get('count_of_letters') is None:
    request.session['count_of_letters'] = number_of_letters
    letter_must = request.POST["wanted_letter"].lower()
    #if request.session.get('letters_needed') is None:
    request.session['letters_needed'] = letter_must
    letter_unwanted = request.POST["unwanted_letter"].lower()
    #if request.session.get('letters_dont_needed') is None:
    request.session['letter_dont_needed'] = letter_unwanted
    chosen_words = []
    context = {}
    for word in words_list:
        if len(word) == number_of_letters and all(ch in word for ch in letter_must) and all(ch not in word for ch in letter_unwanted):
            chosen_words.append(word)
    if chosen_words == []:
        context['words'] = 'no word found'
    else:
        context['words'] = chosen_words
    
    return render(request, 'hello/display_word.html', context)


def reset_ (request):
    request.session['count_of_letters'] = ''
    request.session['letters_needed'] = ''
    request.session['letter_dont_needed'] = ''
    return render(request, "hello/enter.html")
    