from django.urls import path
from . import views

urlpatterns = [
    path("scorecard/", views.show_scorecard, name='show_scorecard'),
    path('icrease_sc/<int:id>', views.change_scoreboard, name='change_scoreboard')
]