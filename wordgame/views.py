from django.shortcuts import render
from django.http import HttpResponse

def say_hello(request):
    return HttpResponse('''
    <!doctype html>
    <html>
        <body>
            <h3>Greetings</h3>
            <p>Hello, World!</p>
        </body>
    </html>''')

def show_html (request):
    return render(request, 'image.html')
