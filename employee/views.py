from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Employee, Employee_attendance

def date_(request):
    if request.method == 'POST':
        date_ = request.POST['date']
        return HttpResponse(f'''
    <!doctype html>
    <html>
        <body>
            <h3>Rock-Paper-Scissors</h3>
            <p>date: {date_}</p>
        </body>
    </html>
    ''')  
    else:
        return render(request, 'employee/somu.html')



def Employee_list (request):
    Employees = Employee.objects.all()
    context = {
        "employee" : Employees
    }
    return render(request, 'employee/employee_list.html', context)


#adding Employee

def add_employee(request):
    if request.method == "GET":
        return render(request, 'employee/add_employee.html')
        
    else:
        e_name_ = request.POST['name']
        e_id_ = request.POST['eid']
        salary_ = request.POST['salary']

        emp = Employee(
            e_name = e_name_,
            e_id = e_id_,
            salary = salary_
        )
        emp.save()
        return redirect('employee_list')

#showing employee details

def employe_detail(request , id):
    emp = Employee.objects.get(id=id)
    emp_attendance = Employee_attendance.objects.filter(employee =emp)
    context = {
        'employee_' : emp,
        'emp_attendance' : emp_attendance
    }
    return render(request, 'employee/emp_detail.html' , context)

    

#deleting employee 

def delete_employee(request, id):
    emp = Employee.objects.get(id = id)
    emp.delete()
    return redirect('employee_list')

#modifying employee

def modify_employee(request , id):
    if request.method == 'POST':
        emp = Employee.objects.get(id= id)
        emp.e_name = request.POST['e_name']
        emp.e_id = request.POST['eid']
        emp.salary = request.POST['salary']
        emp.save()
        return redirect('employee_details', id)

    elif request.method == 'GET':
        emp = Employee.objects.get(id = id)
        condition = True
        context = {
            'employee' : emp,
            "editing" : condition
        }
        return render(request, 'employee/emp_detail.html', context)

    

def add_employee_details(request, id):
    emp = Employee.objects.get(id=id)
    new_date = request.POST['date']
    new_attendance = request.POST['attendance']
    emp_ = Employee_attendance.objects.create(\
            employee = emp,
            attendance = new_attendance,
            date = new_date
        )
    return redirect('employee_details', id)

def modify_employee_attendance(request, id, emp_id):
    emp = Employee_attendance.objects.get(id=emp_id)
    if request.method == 'GET':
        context = {
            'emp' :emp
        }
        return render(request, 'employee/edit_employee_attendance.html', context)
    elif request.method == 'POST':
        new_date = request.POST['date']
        attendance_ = request.POST['attendance']
        emp.date = new_date
        emp.attendance = attendance_
        emp.save()
        return redirect('employee_details', id)