from django.urls import path
from . import views

urlpatterns = [
    path("allproduct/", views.products_list, name='products_list'),
    path('addproduct/', views.add_product, name = "add_employee"),
    path('productdet/<int:id>', views.product_detail, name="product_details"),
    path('productupd/<int:id>', views.modify_product, name="modify_product"),
    path('productdel/<int:id>',views.delete_product, name= "delete_product")
]
