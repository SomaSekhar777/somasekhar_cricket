from django.db import models

# Create your models here.
class Employee(models.Model):
    e_name = models.CharField(max_length = 100)
    e_id = models.CharField(max_length = 10)
    salary = models.CharField(max_length = 30)

    def __str__(self):
        return f'{self.e_name} - {self.e_id} - {self.salary}'


class Employee_attendance(models.Model):
    date = models.DateField()
    attendance = models.CharField(max_length=10)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.employee.e_id} - {self.date} - {self.attendance}'




