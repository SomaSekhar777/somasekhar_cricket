from django.shortcuts import render, redirect
from ecommerce.models import Product

#Displaying products list:
def products_list (request):
    products = Product.objects.all()
    context  = {
        "products" : products
    }
    return render(request )

def add_product(self):
    pass


def product_detail(self, id):
    pass

def modify_product(self, id):
    pass

def delete_product(self, id):
    pass

