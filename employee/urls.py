from django.urls import path
from . import views

urlpatterns = [
    path('somu/', views.date_, name="somu"),
    path('allemp/',views.Employee_list, name = "employee_list"),
    path('addemp/', views.add_employee, name = "add_employee"),
    path('empdet/<int:id>', views.employe_detail, name="employee_details"),
    path('empupd/<int:id>', views.modify_employee, name="modify_employee"),
    path('empdel/<int:id>',views.delete_employee, name= "delete_employee"),
    path('add_employee_details/<int:id>', views.add_employee_details, name="add_employee_details"),
    path('add_employee_details/<int:id>/modify_employee_attendance/<int:emp_id>', views.modify_employee_attendance, name='modify_employee_attendance')
]

