from django.db import models

class Product (models.Model):

    product_name = models.CharField(max_length= 200)
    product_price = models.CharField(max_length= 20)
    product_rating = models.CharField(max_length = 6)
    product_description = models.CharField(max_length = 5000)
    products_quantity = models.CharField(max_length= 10)

    def __str__(self):
        return f'{self.product_name} - {self.product_price} - {self.product_rating}- {self.product_description}- {self.products_quantity}'


