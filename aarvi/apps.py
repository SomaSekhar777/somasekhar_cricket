from django.apps import AppConfig


class AarviConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'aarvi'
