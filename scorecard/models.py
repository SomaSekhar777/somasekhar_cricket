from django.db import models

class Teams(models.Model):
    country_name = models.CharField(max_length=100)

class Score(models.Model):
    total_runs = models.IntegerField()
    total_wickets = models.IntegerField()
    total_overs = models.FloatField()
    country = models.ForeignKey(Teams, on_delete=models.CASCADE)

