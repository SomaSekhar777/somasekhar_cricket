from django.shortcuts import render, redirect
from scorecard.models import Teams , Score


def show_scorecard(request):
    team = Teams.objects.get(id= 1)
    team_score = Score.objects.get(country = team)
    team_score.save()
    context = {
        'teams' : team,
        'score' : team_score
    } 
    return render(request, 'scorecard/index.html', context)

def change_scoreboard(request, id):
    team = Teams.objects.get(id= id)
    team_score = Score.objects.get(country = team)
    if request.POST.get('4') == '4':
        team_score.total_runs += 4
        team_score.total_overs += .1 
        team_score.save()
    elif request.POST.get('w') == 'WICKET':
        #if team_score.total_wickets = 0
        team_score.total_wickets += 1
        team_score.total_overs += .1 
        team_score.save()
        #else:
            #team_score.total_wickets = 0
            #team_score.save()
            
    elif request.POST.get('6') == '6':
        team_score.total_runs += 1
        team_score.total_overs += .1 
        team_score.save()
    elif request.POST.get('1') == '1':
        team_score.total_runs += 1
        team_score.total_overs += .1 
        team_score.save()
    elif request.POST.get('reset') == 'reset':
        team_score.total_runs = 0
        team_score.total_wickets = 0
        team_score.total_overs = 0 
        team_score.save()
    
    return redirect('show_scorecard')


        

