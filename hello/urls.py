from django.urls import path
from . import views

urlpatterns = [
    path("sayhello/", views.say_hello, name = 'say_hello'),
    path('rps/<str:user_choice>', views.play_rps, name='play_rps'),
    path('quiz/form', views.show_question, name='show_question'),
    path('quiz/formpro', views.show_question, name= "process_quiz_form"),
    
    #path('quiz/form', views.show_question_, name='show_question'),
    #path('quiz/formpro', views.process_quiz_form_, name= "process_quiz_form"), 
    #path('line/', views.text_viewer, name= "somasekhar"),
    path('input/', views.word_, name = 'input'),
    path('output/',views.process_, name = 'output'),
    path('reset/',views.reset_, name = 'reset_')

]